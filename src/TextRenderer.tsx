import React, { useEffect, useMemo, useRef, useState } from "react"
import { useControl } from "react-three-gui"
import * as THREE from "three"
import { TextBufferGeometry, TextGeometryParameters } from "three"
import { useBox } from "use-cannon"

// @ts-ignore

function Text({ text, font }: { text: string; font: THREE.Font }) {
  //   const textPosX = useControl("x", {
  //     type: "number",
  //     group: "text",
  //     value: 3,
  //     min: -5,
  //     max: 5,
  //   })
  //   const textPosY = useControl("y", {
  //     type: "number",
  //     group: "text",
  //     value: 2,
  //     min: -5,
  //     max: 5,
  //   })

  const color = useControl("color", {
    group: "text",
    type: "color",
    value: "#0B8D85",
  })
  const geometryRef = useRef<TextBufferGeometry | null>(null)
  //   const [boundingBox, setBoundingBox] = useState<THREE.Box3 | null>(null)
  const [ref, api] = useBox(() => {
    //   const size = boundingBox

    // geometryRef.current?.center()
    geometryRef.current?.computeBoundingBox()
    const size = geometryRef.current?.boundingBox
    console.log("SIZE", size, geometryRef.current)
    //   debugger
    if (!size) {
      throw new Error("cannot measure text size!")
    }

    return {
      mass: 3,
      //   rotation: [0, Math.PI * rotationY, 0],
      //   angularDamping: 1,
      velocity: [0, -10, 0],
      angularVelocity: [0.1, 0.1, 0.2],
      linearFactor: [0, 1, 0],
      material: { friction: 10 },
      angularFactor: [0, 0, 1], // do not fall over
      args: [
        // TODO: why /2 ???
        (size.max.x - size.min.x - 0.1) / 2,
        (size.max.y - size.min.y - 0.1) / 2,
        (size.max.z - size.min.z - 0.1) / 2,
      ],

      //   scale: [scale * -1, scale * -1, scale * -1],
      position: [1, 10, 0],
    }
  }, undefined)
  const [physicsReady, setPhysicsReady] = useState(false)
  useEffect(() => {
    const sub = api.position.subscribe((pos) => {
      setPhysicsReady(true)
    })
    return () => {
      // @ts-ignore
      sub()
    }
  }, [api])

  //   const wireframeRef = useRef<Mesh | null>(null)
  //   useEffect(() => {
  //     const sub1 = api.rotation.subscribe(([x, y, z]: number[]) => {
  //       wireframeRef.current?.rotation.set(x, y, z)
  //     })
  //     const sub2 = api.position.subscribe(([x, y, z]: number[]) => {
  //       wireframeRef.current?.position.set(x, y, z)
  //     })
  //     return () => {
  //       // @ts-ignore
  //       sub1()
  //       // @ts-ignore
  //       sub2()
  //     }
  //   }, [api])
  //   useFrame((state, delta) => {
  //     if (wireframeRef.current && ref.current) {
  //       const { x, y, z } = ref.current.position
  //       const wireframe = wireframeRef.current
  //       wireframe.position.set(x, y, z)
  //       // @ts-ignore
  //       wireframe.quaternion.set(...ref.current.quaternion.toArray())
  //       const bb = geometryRef.current?.boundingBox
  //       if (bb) {
  //         wireframe.scale.x = bb.max.x - bb.min.x
  //         wireframe.scale.y = bb.max.y - bb.min.y
  //         wireframe.scale.z = bb.max.z - bb.min.z
  //       }
  //       wireframe.visible = false
  //     }
  //   })

  const textArgs: [string, TextGeometryParameters] = useMemo(() => {
    return [
      text,
      {
        font: font,
        size: 1,

        height: 0.3,
        curveSegments: 3,
        bevelEnabled: true,
        bevelThickness: 0.1,
        bevelSize: 0.1,
        bevelOffset: 0,
        bevelSegments: 3,
      },
    ]
  }, [font, text])

  return (
    <>
      {/* <mesh ref={wireframeRef}>
        <boxGeometry
          attach="geometry"
          //   args={[
          //     boundingBox.max.x - boundingBox.min.x,
          //     boundingBox.max.y - boundingBox.min.y,
          //     boundingBox.max.z - boundingBox.min.z,
          //   ]}
        />
        <meshBasicMaterial color="rgba(255,0,0,0.5)" />
      </mesh> */}
      <mesh
        key={text}
        ref={ref}
        castShadow
        position={[0, 1000, 0]}
        onClick={() => {
          api.applyImpulse([0, 30, 0], ref.current!.position.toArray())
        }}
        //   rotation={[Math.PI * rotationX, Math.PI * rotationY, 0]}
        // scale={[scale * -1, scale * -1, scale * -1]}
        // position={[3, 3, 0]}
        //   position={[textPosX, textPosY, 0]}
      >
        <textBufferGeometry
          onUpdate={(t) => {
            t.center()
            t.computeBoundingBox() // update
          }}
          ref={geometryRef}
          //   ref={(t: null | TextBufferGeometry) => {
          //     if (t) {
          //       // t.center()
          //       console.log("text", t)

          //       t.center()
          //       t.computeBoundingBox() // update
          //       const { boundingBox } = t
          //       console.log("UPDATE", boundingBox)
          //       setBoundingBox((previous) => {
          //         if (boundingBox && previous?.equals(boundingBox)) {
          //           return previous
          //         } else {
          //           return boundingBox
          //         }
          //       })
          //     }
          //   }}
          attach="geometry"
          args={textArgs}
        />
        {/* <meshBasicMaterial attach="material" color={color} wireframe /> */}
        <meshStandardMaterial
          attach="material"
          color={color}
          metalness={0.1}
          roughness={0.6}
          visible={physicsReady}

          // emissive={new THREE.Color("green")}
          // emissiveIntensity={0.5}
        />
      </mesh>
    </>
  )
}

export function TextRenderer({ text }: { text: string }) {
  const [font, setFont] = useState<THREE.Font | null>(null)
  useEffect(() => {
    new THREE.FontLoader().load("/Luckiest-Guy_Regular.json", setFont)
  }, [])

  if (!font) {
    return null
  }
  return <Text font={font} text={text} />
}
