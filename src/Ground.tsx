import React from "react"
import { useControl } from "react-three-gui"

import { usePlane } from "use-cannon"

export function Ground({
  type,
  color = "#ccc",
  ...props
}: {
  position: [number, number, number]
  type?: "horizontal" | "vertical"
  color?: string
}) {
  const rotationX: number = type === "vertical" ? 0 : Math.PI / 2

  const [ref] = usePlane(() => ({
    ...props,
    material: {
      friction: 1.1,
    },
    mass: 0, // static
    rotation: [-rotationX, 0, 0],
  }))

  return (
    <mesh ref={ref} receiveShadow>
      <planeGeometry attach="geometry" args={[200, 200, 200]} />
      <meshPhysicalMaterial attach="material" color={color} />
    </mesh>
  )
}
