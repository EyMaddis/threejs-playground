// @refresh reset
import React, { useRef } from "react"
import { Canvas, useFrame, useThree } from "react-three-fiber"
import { Controls, useControl } from "react-three-gui"
import * as THREE from "three"
import { PointLight } from "three"
import { Physics } from "use-cannon"
import "./App.css"
import { Box } from "./Box"
import { DraggableCameraControls } from "./DraggableCameraControls"
import { Effect } from "./Effect"
import { Ground } from "./Ground"
import { TextRenderer } from "./TextRenderer"

const query: { [key in string]: string } = (function parseQuery() {
  const queryString = window.location.search
  const query = {}
  const pairs = (queryString[0] === "?"
    ? queryString.substr(1)
    : queryString
  ).split("&")
  for (let i = 0; i < pairs.length; i++) {
    const pair = pairs[i].split("=")
    // @ts-ignore
    query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || "")
  }
  return query
})()

function CameraLight() {
  const ref = useRef<PointLight>()
  const { camera } = useThree()
  const lightIntensity = useControl("Intensity", {
    type: "number",
    group: "cameraLight",
    value: 0.5,
    max: 2,
  })
  const lightColor: THREE.Color = useControl("Intensity", {
    type: "color",
    group: "cameraLight",
    value: "yellow",
  })
  useFrame(() => {
    ref.current!.position.copy(camera.position)
  })
  return (
    <pointLight
      ref={ref}
      position={camera.position}
      intensity={lightIntensity}
      color={lightColor}
    />
  )
}

function Cool() {
  const activateBoxes: boolean = useControl("Boxes?", {
    type: "boolean",
    value: false,
  })

  const text = useControl("Text", {
    type: "string",
    group: "text",
    value: query.text || "Moin Moin",
  })
  const groundColor = useControl("GroundColor", {
    type: "color",
    value: "#B66433",
  })

  const cameraLight: boolean = useControl("enable?", {
    type: "boolean",
    value: false,
    group: "cameraLight",
  })
  const backgroundLight: boolean = useControl("enable?", {
    type: "boolean",
    value: true,
    group: "backgroundLight",
  })
  const backgroundLightIntensity = useControl("intensity", {
    type: "number",
    value: 10,
    group: "backgroundLight",
    max: 100,
  })
  const backgroundLightDistance = useControl("distance", {
    type: "number",
    value: 30,
    max: 100,
    group: "backgroundLight",
  })
  const backgroundLightColor = useControl("color", {
    type: "color",
    value: groundColor,
    group: "backgroundLight",
  })

  const cameraPos: [number, number, number] = [0, 2, 5]
  return (
    <Canvas
      camera={{
        position: cameraPos,
      }}
      shadowMap
      onCreated={({ gl, scene }) => {
        gl.shadowMap.enabled = true
        gl.shadowMap.type = THREE.PCFSoftShadowMap
        // @ts-ignore
        window.scene = scene
      }}
    >
      <Physics
        defaultContactMaterial={{
          friction: 1.9,
          restitution: 0.7,
          contactEquationStiffness: 1e7,
          contactEquationRelaxation: 1,
          frictionEquationStiffness: 1e7,
          frictionEquationRelaxation: 2,
        }}
      >
        {activateBoxes ? (
          <>
            <Box position={[-1.5, 1, 0]} />
            <Box position={[-2.5, 1, 0]} />
            <Box position={[-2, 2, 0]} />
            <Box position={[8, 1, 0]} />
            <Box position={[0, 1, 0]} />
            <Box position={[5, 1, 0.5]} />
            <Box position={[5, 1, 1.5]} />
            <Box position={[1.2, 1.5, 0]} />
          </>
        ) : null}

        <TextRenderer key={text + activateBoxes} text={text} />
        <Ground color={groundColor} position={[0, 0, 0]} type="horizontal" />
        <Ground color={groundColor} position={[0, 0, -7]} type="vertical" />
      </Physics>
      {backgroundLight && (
        <pointLight
          position={[0, 0, -6.9]}
          // castShadow
          color={backgroundLightColor}
          intensity={backgroundLightIntensity}
          distance={backgroundLightDistance}
        />
      )}
      {/* <fog attach="fog" far={20} /> */}
      <ambientLight intensity={0.9} />
      {cameraLight ? <CameraLight /> : null}
      <spotLight
        castShadow
        penumbra={1}
        intensity={0.95}
        angle={Math.PI / 8}
        position={[-25, 25, 10]}
        // shadow-mapSize-width={2048}
        // shadow-mapSize-height={2048}
      />
      <DraggableCameraControls enableDamping autoRotate={false} />
      <Effect />
    </Canvas>
  )
}

function App() {
  return (
    <div className="App">
      <Cool />
      {query.controls ? <Controls /> : null}
    </div>
  )
}

export default App
