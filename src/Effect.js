import React from "react"
import { extend, useThree, useFrame } from "react-three-fiber"
import { useRef, useMemo, useEffect } from "react"
import * as THREE from "three"

import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer"
import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass"
import { AfterimagePass } from "three/examples/jsm/postprocessing/AfterimagePass"
import { FXAAShader } from "three/examples/jsm/shaders/FXAAShader"
import { UnrealBloomPass } from "three/examples/jsm/postprocessing/UnrealBloomPass"

extend({
  EffectComposer,
  ShaderPass,
  RenderPass,
  AfterimagePass,
  FXAAShader,
  UnrealBloomPass,
})

export function Effect() {
  const composer = useRef()
  const { scene, gl, size, camera } = useThree()
  const aspect = useMemo(() => new THREE.Vector2(size.width, size.height), [
    size,
  ])
  useEffect(() => void composer.current.setSize(size.width, size.height), [
    size,
  ])
  useFrame(() => composer.current.render(), 1)
  return (
    <>
      {/*
      // @ts-ignore */}
      <effectComposer ref={composer} args={[gl]}>
        <renderPass attachArray="passes" scene={scene} camera={camera} />
        {/* <waterPass attachArray="passes" factor={2} /> */}
        <afterimagePass attachArray="passes" uniforms-damp-value={0.3} />
        <unrealBloomPass attachArray="passes" args={[aspect, 0.1, 0.1, 0]} />
        <shaderPass
          attachArray="passes"
          args={[FXAAShader]}
          uniforms-resolution-value={[1 / size.width, 1 / size.height]}
          renderToScreen
        />
      </effectComposer>
    </>
  )
}
