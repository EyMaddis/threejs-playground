import { ComponentProps } from "react"

export type MeshProps = ComponentProps<"mesh">
