import React, { useRef } from "react"
import { extend, useFrame, useThree } from "react-three-fiber"
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"

extend({ OrbitControls })
const Comp: any = "orbitControls" // silly fix for https://github.com/facebook/create-react-app/issues/8687
export function DraggableCameraControls(props: Partial<OrbitControls>) {
  const { gl, camera } = useThree()
  const ref = useRef<OrbitControls>()
  useFrame(() => ref.current!.update())

  return (
    <Comp
      ref={ref}
      args={[camera, gl.domElement]}
      {...props}
      maxPolarAngle={Math.PI / 2 - 0.1} //  slightly above ground
    />
  )
}
