import React, { useEffect, useState, useMemo, useRef } from "react"
import * as THREE from "three"
import { useBox } from "use-cannon"
import { useFrame } from "react-three-fiber"
import { MeshPhysicalMaterial } from "three"

export function Box({
  size = 1,
  ...props
}: {
  size?: number
  position: [number, number, number]
}) {
  // This reference will give us direct access to the mesh

  // Set up state for the hovered and active state
  const [hovered, setHover] = useState(false)
  const [active, setActive] = useState(false)

  const [ref, api] = useBox(() => ({
    mass: 1,
    material: { friction: 5 },
    ...props,
  }))
  //   useEffect(() => {
  //     api.allowSleep?.set(false)
  //   }, [])

  //   useEffect(() => {
  //     const i = setInterval(() => {
  //       if (ref.current) {
  //         const force = [0, 100, 0] as const
  //         const at = ref.current.position.toArray()
  //         console.log("BOOM", force, at)
  //         api.allowSleep?.set(false)
  //         api.velocity.set(0, 30, 0)
  //       }
  //     }, 3000)
  //     return () => clearInterval(i)
  //   }, [])

  // Rotate mesh every frame, this is outside of React without overhead
  // useFrame(() => {
  //   return (mesh.current!.rotation.y += 0.01)
  // })
  const color = useMemo(() => {
    const rand = () => Math.round(75 + Math.random() * 100)
    const grey = rand() // minValue 50, maxValue 200, never white, never black
    return new THREE.Color(`rgb(${rand()},${rand()},${rand()})`)
  }, [])
  const materialRef = useRef<MeshPhysicalMaterial | null>(null)
  useFrame(() => {
    const m = materialRef.current!
    m.opacity = 1
    m.transparency = 0.5
  })
  return (
    <mesh
      ref={ref}
      {...props}
      onClick={(...args) => {
        console.log("click", props.position)
        api.applyLocalForce([0, 150, 0], [0, 0, 0])
        // api.angularVelocity.set(0, 0, 20)
        setActive(!active)
      }}
      //   onPointerOver={(e) => setHover(true)}
      //   onPointerOut={(e) => setHover(false)}
      castShadow
      receiveShadow
    >
      <boxBufferGeometry attach="geometry" args={[size, size, size]} />
      <meshPhysicalMaterial
        ref={materialRef}
        attach="material"
        color={hovered ? "hotpink" : color}
      />
    </mesh>
  )
}
